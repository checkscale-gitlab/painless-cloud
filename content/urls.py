"""
URL configuration for content app.
"""
from django.conf.urls import url
from django.views.generic import TemplateView

urlpatterns = [
    url(r'^how-it-works',
        TemplateView.as_view(template_name='content/how-it-works.html'),
        name='content-howitworks'),
    url(r'^features',
        TemplateView.as_view(template_name='content/features.html'),
        name='content-features'),
    url(r'^pricing',
        TemplateView.as_view(template_name='content/pricing.html'),
        name='content-pricing'),
]
